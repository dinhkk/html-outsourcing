<!DOCTYPE html>
<html>
<head lang="it">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <!--<BASE href="http://localhost/html-outsourcing/">-->
    <BASE href="http://outsourcing.teusoft.dk">
    <title>Affittiamo la flessibilità dei nostri developer e programmatori</title>
    <meta name="description" content="Affitta in Vietnam il tuo developer o programmatore personale con supporto europeo">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/1200px_12_columns_30px_gutter.css">
    <link rel="stylesheet" href="./css/style.css">


    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    <script src="./js/jquery.malihu.PageScroll2id.min.js"></script>
    <script src="./js/script.js"></script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

</head>
<body>
<!-- Slideshow-->
<div id="header">
    <?php include('slider.php'); ?>
</div>

<!-- Global Navigation, including logo -->
<div id="top">
    <div class="container-12">
        <div class="group">
            <div class="grid-12" id="nav">
                <div id="logo"><img alt="" src="images/teu-logo-01.svg" ><a href="./it/#">TeuSoft Outsourcing</a></div>
                <a id="nav-trigger" href="javascript:;"><img alt="=" src="images/menu.svg"></a>
                <ul>
                    <li><a href="./it/#rent-developer" rel='m_PageScroll2id'>Affitta un developer</a></li>
                    <li class="selected"><a href="./it/#skill-list" rel='m_PageScroll2id'>Prezzi</a></li>
                    <li><a href="./it/#flexibility-team" rel='m_PageScroll2id'>Servizi</a></li>
                    <li><a href="./it/#qualification" rel='m_PageScroll2id'>Competenze</a></li>
                    <li><a href="./it/#about_us" rel='m_PageScroll2id'>Chi siamo</a></li>
                    <li><a href="./it/#contact-form" rel='m_PageScroll2id'>Contatti</a></li>
                    <li id="flag">
                        <a id="current-flag" href="javascript:;"><img alt="English" src="images/gb_16x16.png"></a>
                        <div>
                            <a href="./da/"><img alt="Danish" src="images/dk_16x16.png"></a>
                            <a href="./"><img alt="English" src="images/gb_16x16.png"></a>
                            <a href="./it/"><img alt="Italian" src="images/it_16x16.png"></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- MAIN HOME PAGE -->
<div id="home">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Un’azienda su 5 in Europa lavora con l’outsourcing! <strong>E tu?</strong></h2>
                    <p><strong>Qualità europea.</strong> Verifichiamo le qualifiche dei nostri developer prima di assumerli e li formiamo con istruttori esperti europei per potenziarne l’efficienza e la precisione nel lavoro.
                        Selezioniamo solo i migliori talenti in modo da garantirti la massima qualità.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER TITLE -->
<div id="rent-developer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><h2 class="sub-head-line"> Affittiamo i nostri developer e programmatori</h2></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER CONTENT -->
<div id="rent-a-developer">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-desktop fa-fw fa-2x"></i></span>
                        <p>Sviluppo di applicazioni web, siti ed e-commerce.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-mobile fa-fw fa-2x"></i></span>
                        <p>App per smartphone e tablet.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-code fa-fw fa-2x"></i></span>
                        <p>Programmazione hardware.</p>
                    </div>
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-globe fa-fw fa-2x"></i></span>
                        <p>Qualità e semplicità europea con forza lavoro a costi ridotti.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-money fa-fw fa-2x"></i></span>
                        <p>Prezzi fissi trasparenti tutto incluso.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-group fa-fw fa-2x"></i></span>
                        <p>Accesso all’utilizzo di altri developer del team.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-12 margin50">
                <div class="inner centerize">
                    <a class="btn" rel="m_PageScroll2id" href="./it/#contact-form" rel='m_PageScroll2id'>Ordina la brochure</a>
                </div>
            </div>
            <div class="grid-12"><img id="img-competency" alt="" src="images/competency.png"></div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER FOOTER -->
<div id="rent-developer-footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><p>La nostra esperienza è di lungo periodo! I nostri developer lavorano con sistemi che tu e la tua azienda utilizzate.
                        Abbiamo esperti in e-commerce, SEO e API per Wordpress e Magento e anche in sistemi di amministrazione per ricerche e gestione progetti.</p></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT FULL TIME DEVELOPER WITH IMAGE -->
<div id="rent-fulltime-developer-img" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Affitta uno developer a meno di <br><span>14€ l’ora..</span></h2>
                    <h2 class="sub-head-line">Crea il tuo pacchetto in base alle tue esigenze con la possibilità di impiego part-time.</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- LIST SKILLS -->
<div id="skill-list">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <div class="col">
                        <ul>
                            <li class="head-table">Pay as you go</li>
                            <li class="price-cell">23,25€<span>ora</span></li>
                            <li>40 ore al mese</li>
                            <li>Developer dedicato</li>
                            <li>Incl. Team Leader</li>
                            <li>1 ora di supporto*</li>
                            <li>Trasferisci 40 ore**</li>
                            <li>Ore extra per 25€/h<br>Straordinario per 30€/h</li>
                            <li>Minimo 3 mesi***</li>
                            <li>Su 6 mesi hai 20 ore gratis<br>Su 12 mesi hai 40 ore gratis<br>&nbsp;</li>
                            <li>929€ / mese</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Ordina ora</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Halftime</li>
                            <li class="price-cell">15,75€<span>ora</span></li>
                            <li>880 ore al mese</li>
                            <li>Developer dedicato</li>
                            <li>Incl. Team Leader</li>
                            <li>2 ore di supporto*</li>
                            <li>Trasferisci 40 ore**</li>
                            <li>Ore extra per 16€/h<br>Straordinario per 23€/h</li>
                            <li>Minimo 3 mesi***</li>
                            <li>Su 6  mesi hai 40 ore gratis<br/>Su 12 mesi hai 80 ore gratis<br>&nbsp;</li>
                            <li>1459€ / mese</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Ordina ora</a></li>
                        </ul>
                    </div>
                    <div class="col featured">
                        <ul>
                            <li class="head-table">Fulltime</li>
                            <li class="price-cell">13,30€<span>ora</span></li>
                            <li>160 ore al mese</li>
                            <li>Developer dedicato</li>
                            <li>Incl. Team Leader</li>
                            <li>4 ore di supporto*</li>
                            <li>Trasferisci 80 ore**</li>
                            <li>Ore extra per 14€/h<br>Straordinario per 18€/h</li>
                            <li>Minimo 3 mesi***</li>
                            <li><strong>Get 40 hours free</strong><br>Su 6  mesi hai 80 ore gratis<br>Su 12  mesi hai 160 ore gratis</li>
                            <li>2129€ / mese</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Ordina ora</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Special</li>
                            <li class="price-cell">Speciale<span>&nbsp;</span></li>
                            <li>Dedicato</li>
                            <li>Developer dedicati</li>
                            <li>Incl. Team Leader</li>
                            <li>Secondo esigenze</li>
                            <li>Trasferisci 80 ore <span class="small">per developer**</span></li>
                            <li>Preventivo personalizzato<br>&nbsp;</li>
                            <li>Minimo 3 mesi***</li>
                            <li>Sconto speciale<br>&nbsp;<br>&nbsp;</li>
                            <li>Speciale</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Richiedi un preventivo</a></li>
                        </ul>
                    </div>
                </div>
                <div class="inner explanation">
                    <p>*Supporto avanzato in italiano. Supporto aggiuntivo può essere acquistato per 55€ / ora</p>
                    <p>**Le ore lavorative non utilizzate posso essere trasferite al mese successivo.</p>
                    <p>***Contratto minimo di 3 mesi. Cancellazione con 2 mesi di preavviso.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Flexibility team -->
<div id="flexibility-team" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">La flessibilità è la nostra parola chiave!</h2>
                    <p>Quando affitti un nostro developer hai anche accesso all’intero team. Se all’improvviso hai un’idea straordinaria o la necessità di supporto ulteriore, ricevi aiuto da developer con altre competenze.
                        In questo modo non hai bisogno di assumere altro personale, trovare un consulente o acquistare supporto extra. Il pacchetto è onnicomprensivo.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SERVICES -->
<div id="services">
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <h2 class="head-line">Supporto sempre in italiano con qualità e project management europei.</h2>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-pie-chart fa-2x"></i>
                    <ul>
                        <li>Il tuo developer dedicato per un prezzo fisso mensile.</li>
                        <li>Gestiscilo come un tuo dipendente.</li>
                        <li>Decidi tu le competenze del tuo dipendente.</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Scopri di più</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-area-chart fa-2x"></i>
                    <ul>
                        <li>Possibilità di provare per 14 giorni a tariffa ridotta.</li>
                        <li>Utilizza il tecnico quando ne hai bisogno.</li>
                        <li>Prezzi fissi contenuti, nessun altro coso.</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Scopri di più</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-line-chart fa-2x"></i>
                    <ul>
                        <li>Usaci come un’agenzia</li>
                        <li>Curiamo il tuo progetto dall’inizio alla fine</li>
                        <li>Progetti per un tempo e prezzo fisso <br>&nbsp;</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./it/#contact-form">Richiedi un preventivo gratis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- QUALIFICATION -->
<div id="qualification" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Siamo maestri in tecnologia</h2>
                    <p>E utilizziamo solo le più avanzate tecnologie e metodologie di lavoro</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="technology">
    <div class="container-12">
        <div class="group">
            <div class="grid-5 offset-1">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-arrows-h"></i>Responsive design</li>
                        <li><i class="fa fa-html5"></i>HTML5 & CSS3</li>
                        <li><i class="fa fa-connectdevelop"></i>Javascript & jQuery</li>
                        <li><i class="fa fa-code"></i>API's</li>
                        <li><i class="fa fa-bug"></i>OOP & (H)MVC</li>
                        <li><i class="fa fa-git"></i>Git/SVN</li>
                        <li><i class="fa fa-puzzle-piece"></i>Numerosi framework</li>
                        <li><i class="fa fa-server"></i>Innumerevoli sistemi amministrativi</li>
                    </ul>
                </div>
            </div>
            <div class="grid-5">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-picture-o"></i>Frontend, SEO e graphic design</li>
                        <li><i class="fa fa-apple"></i>iOS development</li>
                        <li><i class="fa fa-android"></i>Android development</li>
                        <li><i class="fa fa-check"></i>PHP</li>
                        <li><i class="fa fa-wrench"></i>.NET & C#</li>
                        <li><i class="fa fa-bolt"></i>Node.js</li>
                        <li><i class="fa fa-terminal"></i>Python</li>
                        <li><i class="fa fa-codepen"></i>E molto ancora</li>
                    </ul>
                </div>
            </div>
            <div class="grid-12 centerize margin50" id="about_us">
                <div class="inner">
                    <h2 class="head-line">La comunicazione è il nostro servizio più importante!</h2>
                    <p>Come nostro cliente siamo sempre in contatto diretto con te. Abbiamo scadenze precise e facciamo anche gli straordinari per raggiungere l’obiettivo.
                        Decidi tu di quanto supporto hai bisogno. Saremo sempre a portata di telefono e email.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- MEET THE TEAM -->
<div id="meet-the-team" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Il nostro team</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- EXPERT -->
<div id="expert">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <img alt="expert team" src="images/expert.png">
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Conosci gli esperti</h2>
                    <p>Abbiamo ricercato top senior developer, business developer e project manager per condividere le loro competenze e creare i migliori prodotti online. Le nostre parole d’ordine sono comunicazione, flessibilità e fiducia.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="meet-the-team-desc" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Dal 2005</h2>
                    <p>sviluppiamo e-commerce, App, software e web per aziende europee.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT BJARKE -->
<div id="contact-us">
    <div class="container-12">
        <div class="group">
            <div class="grid-7">
                <div class="inner">
                    <h2>Sono la persona con cui sarai in contatto e sarò felice di discutere con te <i class="fa fa-smile-o"></i></h2>
                    <ul>
                        <li><i class="fa fa-user"></i>Bjarke B Rubeksen</li>
                        <li><i class="fa fa-phone"></i>+45 65 74 04 74 [Denmark]</li>
                        <li><i class="fa fa-phone"></i>+84 123 524 6008 [Vietnam]</li>
                        <li><i class="fa fa-envelope"></i><a href="mailto:bjarke@teusoft.dk">bjarke@teusoft.dk</a></li>
                        <li><i class="fa fa-skype"></i>mydunlet</li>
                        <li><i class="fa fa-facebook-official"></i><a href="https://facebook.com/bjarker">facebook.com/bjarker</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT FORM -->
<div id="contact-form">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Vuoi saperne di più?<br>Contattami.</h2>
                    <p>Questo è tutto e spero sia di tuo interesse. Contattaci per:</p>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i>ricevere la nostra brochure tramite email</li>
                        <li><i class="fa fa-angle-double-right"></i>ottenere lo sconto su un developer dedicato</li>
                        <li><i class="fa fa-angle-double-right"></i>un preventivo gratis sul tuo prossimo progetto.</li>
                        <li><i class="fa fa-angle-double-right"></i>e perchè non ti costa nulla!</li>
                    </ul>
                </div>
            </div>
            <div class="grid-6 margin50">
                <div class="inner">
                    <form id="contact_form" action="" >
                        <input name="name" type="text" placeholder="Nome (optional)">
                        <input name="email" type="text" placeholder="Email (required)">
                        <input type="hidden" name="action" value="do">
                        <input type="hidden" class="" size="40" value="it" name="lang" />
                        <select name="subject">
                            <option selected>Oggetto (optional)</option>
                            <option>Informazioni ordine</option>
                            <option>Affitta developer</option>
                            <option>Preventivo gratis</option>
                            <option>Altro</option>
                        </select>
                        <textarea name="message" placeholder="Il tuo messaggio"></textarea>
                        <p id="message_return"></p>
                        <button onclick="processForm();return false"><i id="loading_submit" class="fa fa-spinner fa-pulse"></i> Contattami</button>
                        <!--<input type="submit" value="Contact me">-->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div id="footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Development Office</h4>
                    1st Floor, N3-N4 Building, Group D<br />
                    Lane 57, Lang Ha Street, Hanoi,<br />
                    Vietnam
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Representative Office</h4>
                    Spinderihallerne<br/>
                    Spinderigade 11E, Studio G14<br/>
                    7100 Vejle <br/>
                    Denmark
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Creative Office</h4>
                    Manila Rivercity - Sta. Ana<br />
                    Rhine building 21th floor, 2111<br />
                    Metro Manila
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER BOTTOM -->
<div id="footer-bottom">
    <div class="container-12">
        <div class="group">
            <div class="inner">
                <ul id="menu-footer" class="x-nav">
                    <li><a rel="m_PageScroll2id" href="./it/#rent-developer" >Affitta un developer</a></li>
                    <li><a rel="m_PageScroll2id" href="./it/#skill-list" >Prezzi</a></li>
                    <li><a rel="m_PageScroll2id" href="./it/#flexibility-team" >Servizi</a></li>
                    <li><a rel="m_PageScroll2id" href="./it/#qualification">Competenze</a></li>
                    <li><a rel="m_PageScroll2id" href="./it/#meet-the-team">Chi siamo</a></li>
                    <li><a rel="m_PageScroll2id" href="./it/#contact-form">Contatti</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <p>Copyright &copy; <?php echo date('Y'); ?> TeuSoft.dk. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div style="" class="x-content-dock left x-content-dock-on" id="widget_dock_wrapper">
    <div class="widget " id="mltwtext-2"><h4 class="h-widget">Hey, Made You Look!</h4>
        <div class="MLTW_content Dinhkk-come-here">
            <div class="textwidget">
                <p>
                    Accedi gratis ad altre informazioni! Ricevi per email la nostra brochure con il dettaglio prezzi.
                </p>
            </div>
            <div lang="en-US" dir="ltr" id="" class="">
                <div class="screen-reader-response"></div>
                <form id="dock_contact_form" action="#" name="">
                    <p>
                        <span class="your-email">
                            <input type="email" class="" placeholder="Email" size="40" value="" name="email" />
                            <input type="hidden" class="" size="40" value="it" name="lang" />
                            <input type="hidden" class="" size="40" value="dock" name="form_type" />
                        </span></p>
                    <p id="message_return_dock"></p>
                    <p><button class="btn" rel="m_PageScroll2id"  onclick="submit_form_dock(); return false;"><i id="loading_submit_dock" class="fa fa-spinner fa-pulse"></i>Ordina ora </button></p>
                </form>
            </div>
        </div>
    </div>
    <a id="close_dock_widget" class="x-close-content-dock" href="#">
        <span>✖</span>
    </a>
</div>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-61637334-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    //we need email function
    function processForm()
    {
        jQuery('#loading_submit').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#contact_form').serialize(),
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            jQuery('#loading_submit').css('display','none');
            jQuery('#message_return').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return').fadeOut()", 6000);
            if (data.error === 0) {
                jQuery('#success_message').show();
                jQuery('#contact_form')[0].reset();
                jQuery('#message_return').addClass('success');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return').addClass('warning');
            }
        });

        return false;
    }

    function submit_form_dock(){
        jQuery('#loading_submit_dock').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#dock_contact_form').serialize(),
            dataType: 'json',
            timeout: 12000
        }).done(function(data){
            console.log(data);
            jQuery('#loading_submit_dock').css('display','none');
            jQuery('#message_return_dock').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return_dock').fadeOut()", 6000);
            if (data.error === 0) {
                //jQuery('#success_message').show();
                jQuery('#dock_contact_form')[0].reset();
                jQuery('#message_return_dock').addClass('success').removeClass('warning');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return_dock').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return_dock').addClass('warning').removeClass('success');
            }
        });

        return false;
    }
</script>
<?php include('tawk_live_chat.php'); ?>
</body>
</html>