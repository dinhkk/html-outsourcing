<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <title>We rent out flexible developers & programmers</title>
    <meta name="description" content="Rent your own developer or programmer with European support in Vietnam">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/1200px_12_columns_30px_gutter.css">
    <link rel="stylesheet" href="css/style.css">


    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    <script src="./js/jquery.malihu.PageScroll2id.min.js"></script>
    <script src="./js/script.js"></script>
    <!--<script src="//vjs.zencdn.net/4.3/video.js"></script>
    <script src="./js/bigvideo.js"></script>-->
    <script src=./js/jquery.onscreen.min.js"></script><?php /**/?>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

</head>
<body>
<!-- Slideshow-->
<div id="header">
    <?php include('slider.php'); ?>
</div>

<!-- Global Navigation, including logo -->
<div id="top">
    <div class="container-12">
        <div class="group">
            <div class="grid-12" id="nav">
                <div id="logo"><img alt="" src="images/teu-logo-01.svg" ><a href="#">TeuSoft Outsourcing</a></div>
                <a id="nav-trigger" href="javascript:;"><img alt="=" src="images/menu.svg"></a>
                <ul>
                    <li><a href="#rent-developer" rel='m_PageScroll2id'>Rent a developer</a></li>
                    <li class="selected"><a href="#skill-list" rel='m_PageScroll2id'>Pricing</a></li>
                    <li><a href="#flexibility-team" rel='m_PageScroll2id'>Services</a></li>
                    <li><a href="#qualification" rel='m_PageScroll2id'>Qualifications</a></li>
                    <li><a href="#about_us" rel='m_PageScroll2id'>About Us</a></li>
                    <li><a href="#contact-form" rel='m_PageScroll2id'>Contact Us</a></li>
                    <li id="flag">
                        <a id="current-flag" href="javascript:;"><img alt="English" src="images/gb_16x16.png"></a>
                        <div>
                            <a href="./da/"><img alt="Danish" src="images/dk_16x16.png"></a>
                            <a href="./"><img alt="English" src="images/gb_16x16.png"></a>
                            <a href="./it/"><img alt="Italian" src="images/it_16x16.png"></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- MAIN HOME PAGE -->
<div id="home">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">One out of 5 companies in Europe are outsourcing! <strong>Are you?</strong></h2>
                    <p><strong>European quality.</strong> Our developers are educated before they are hired
                        by us, and we are still coaching them with European trainers to develop their efficiency and
                        workflow. We only select the best talents so we can assure you the highest quality.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER TITLE -->
<div id="rent-developer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><h2 class="sub-head-line">We rent out developers and programmers</h2></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER CONTENT -->
<div id="rent-a-developer">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-desktop fa-fw fa-2x"></i></span>
                        <p>Development of web applications, websites & webshops.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-mobile fa-fw fa-2x"></i></span>
                        <p>Apps for smartphones & tablets.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-code fa-fw fa-2x"></i></span>
                        <p>Hardware Programming.</p>
                    </div>
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-globe fa-fw fa-2x"></i></span>
                        <p>Easy and cheap workforce with European quality.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-money fa-fw fa-2x"></i></span>
                        <p>Fixed transparent pricing with all inclusive.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-group fa-fw fa-2x"></i></span>
                        <p>Access to use other developers in our team.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-12 margin50">
                <div class="inner centerize">
                    <a class="btn" rel="m_PageScroll2id" href="#contact-form" rel='m_PageScroll2id'>Contact Us</a>
                </div>
            </div>
            <div class="grid-12"><img id="img-competency" alt="" src="images/competency.png"></div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER FOOTER -->
<div id="rent-developer-footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><p>We have been doing this for a long time! Our developers will work
                        in the systems you and your company are using and we have experts in webshops, SEO & API’s to
                        Wordpress & Magento but also in administrative systems for research and project handling.</p></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT FULL TIME DEVELOPER WITH IMAGE -->
<div id="rent-fulltime-developer-img" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Rent a full time developer for under<br><span>14€ per hour</span></h2>
                    <h2 class="sub-head-line">Combine your own package after your needs with the possibility for part-time employment.</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- LIST SKILLS -->
<div id="skill-list">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <div class="col">
                        <ul>
                            <li class="head-table">Pay as you go</li>
                            <li class="price-cell">23,25€<span>per hour</span></li>
                            <li>40 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Incl. Team Leader</li>
                            <li>1 hours support*</li>
                            <li>Transfer 40 work hours**</li>
                            <li>Extra hours for 25€<br>Overtime for 30€</li>
                            <li>Min. 3 month***</li>
                            <li>6 month get 20 hours free<br>12 months get 40 hours free<br>&nbsp;</li>
                            <li>929€ / month</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Book now</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Halftime</li>
                            <li class="price-cell">15,75€<span>per hour</span></li>
                            <li>80 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Incl. Team Leader</li>
                            <li>2 hours support*</li>
                            <li>Transfer 40 work hours**</li>
                            <li>Extra hours for 16€<br>Overtime for 23€</li>
                            <li>Min. 3 month***</li>
                            <li>6 month get 40 hours free <br/>12 months get 80 hours free<br>&nbsp;</li>
                            <li>1459€ / month</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Book now</a></li>
                        </ul>
                    </div>
                    <div class="col featured">
                        <ul>
                            <li class="head-table">Fulltime</li>
                            <li class="price-cell">13,30€<span>per hour</span></li>
                            <li>160 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Incl. Team Leader</li>
                            <li>4 hours support*</li>
                            <li>Transfer 80 work hours**</li>
                            <li>Extra hours for 14€<br>Overtime for 18€</li>
                            <li>Min. 3 month***</li>
                            <li><strong>Get 40 hours free</strong><br>6 month get 80 hours free<br>12 months get 160 hours free</li>
                            <li>2129€ / month</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Book now</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Special</li>
                            <li class="price-cell">Special<span>&nbsp;</span></li>
                            <li>Your needs</li>
                            <li>Dedicated developer(s)</li>
                            <li>Incl. Team Leader</li>
                            <li>Your need</li>
                            <li>Transfer 80 work hours** <span class="small">/dev.</span></li>
                            <li>Special quote for you needs<br>&nbsp;</li>
                            <li>Min. 3 month***</li>
                            <li>Special discount<br>&nbsp;<br>&nbsp;</li>
                            <li>Special</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Contact for quote</a></li>
                        </ul>
                    </div>
                </div>
                <div class="inner explanation">
                    <p>* Advanced support in your own language. Additional support can be bought for 55€ / hour</p>
                    <p>** Unused work hours can be transferred to the next month.</p>
                    <p>*** Minimum 3 months booking. Can be canceled with 2 month notice.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Flexibility team -->
<div id="flexibility-team" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Flexibility is our keyword!</h2>
                    <p>When you hire a developer through us, you
                also get access to the whole team. If you suddenly get a good idea or you are in need of special
                support, you can get help from other developers with another skill set. So you do not need to hire
                more people, find a freelancer or buy extra support. You get the full package.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SERVICES -->
<div id="services">
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <h2 class="head-line">Always English support with Danish quality and project management.</h2>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-pie-chart fa-2x"></i>
                    <ul>
                        <li>Your own dedicated developer for a fixed price per month.</li>
                        <li>Use as your own employee.</li>
                        <li>You decide the qualifications of your worker.</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Learn more</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-area-chart fa-2x"></i>
                    <ul>
                        <li>Possibility for a 14 days trial for a low fixed price.</li>
                        <li>Use the person when you have work.</li>
                        <li>Fixed low price, no extra fees</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">Learn more</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-line-chart fa-2x"></i>
                    <ul>
                        <li>Use us as an office</li>
                        <li>We take care of your project from start to end</li>
                        <li>Projects for a fixed time and price <br>&nbsp;</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="#contact-form">FREE Quote</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- QUALIFICATION -->
<div id="qualification" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">We’re good with techs</h2>
                    <p>And we only use the newest technologies and workflow</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="technology">
    <div class="container-12">
        <div class="group">
            <div class="grid-5 offset-1">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-arrows-h"></i>Responsive design</li>
                        <li><i class="fa fa-html5"></i>HTML5 & CSS3</li>
                        <li><i class="fa fa-connectdevelop"></i>Javascript & jQuery</li>
                        <li><i class="fa fa-code"></i>API's</li>
                        <li><i class="fa fa-bug"></i>OOP & (H)MVC</li>
                        <li><i class="fa fa-git"></i>Git/SVN</li>
                        <li><i class="fa fa-puzzle-piece"></i>Lots of frameworks</li>
                        <li><i class="fa fa-server"></i>Tons of administrative systems</li>
                    </ul>
                </div>
            </div>
            <div class="grid-5">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-picture-o"></i>Frontend, SEO & graphic design</li>
                        <li><i class="fa fa-apple"></i>iOS development</li>
                        <li><i class="fa fa-android"></i>Android development</li>
                        <li><i class="fa fa-check"></i>PHP</li>
                        <li><i class="fa fa-wrench"></i>.NET & C#</li>
                        <li><i class="fa fa-bolt"></i>Node.js</li>
                        <li><i class="fa fa-terminal"></i>Python</li>
                        <li><i class="fa fa-codepen"></i>And lots more</li>
                    </ul>
                </div>
            </div>
            <div class="grid-12 centerize margin50" id="about_us">
                <div class="inner">
                    <h2 class="head-line">Communication is our most important service!</h2>
                    <p>We are always in close dialogue with you as our client. We have tight deadlines and we will also work overtime to get
                        there. You decide how much support you need. We will be ready at the phone and email.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- MEET THE TEAM -->
<div id="meet-the-team" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Meet the team</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- EXPERT -->
<div id="expert">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <img alt="expert team" src="images/expert.png">
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Meet the experts</h2>
                    <p>We sought out the top senior developers, business developers and project managers to share their expertise on building the best online products. Communication, flexibility and trust are our keywords.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="meet-the-team-desc" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Since 2005</h2>
                    <p>We have been working with webshops, Apps, software and web
                        development for European companies.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT BJARKE -->
<div id="contact-us">
    <div class="container-12">
        <div class="group">
            <div class="grid-7">
                <div class="inner">
                    <h2>It’s me you will be in contact with and I am really looking forward to talk to you <i class="fa fa-smile-o"></i></h2>
                    <ul>
                        <li><i class="fa fa-user"></i>Bjarke B Rubeksen</li>
                        <li><i class="fa fa-phone"></i>+45 65 74 04 74 [DK]</li>
                        <li><i class="fa fa-phone"></i>+84 123 524 6008 [VN]</li>
                        <li><i class="fa fa-envelope"></i><a href="mailto:bjarke@teusoft.dk">bjarke@teusoft.dk</a></li>
                        <li><i class="fa fa-skype"></i>mydunlet</li>
                        <li><i class="fa fa-facebook-official"></i><a href="https://facebook.com/bjarker">facebook.com/bjarker</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT FORM -->
<div id="contact-form">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Want to know more?<br>Contact me.</h2>
                    <p>So you reached the bottom and i hope you feel interested. Contact us for:</p>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i>Our white-paper directly in your mailbox</li>
                        <li><i class="fa fa-angle-double-right"></i>Discount on a dedicated developer</li>
                        <li><i class="fa fa-angle-double-right"></i>FREE quote on your next project.</li>
                        <li><i class="fa fa-angle-double-right"></i>It does not cost you anything!</li>
                    </ul>
                </div>
            </div>
            <div class="grid-6 margin50">
                <div class="inner">
                    <form id="contact_form" action="" >
                        <input name="name" type="text" placeholder="Your Name (optional)">
                        <input name="email" type="text" placeholder="Your Email (required)">
                        <input type="hidden" name="action" value="do">
                        <select name="subject">
                            <option selected>Subject (optional)</option>
                            <option>Order info</option>
                            <option>Rent developer</option>
                            <option>Free quote</option>
                            <option>Other</option>
                        </select>
                        <textarea name="message" placeholder="Your Message"></textarea>
                        <p id="message_return"></p>
                        <button onclick="processForm();return false"><i id="loading_submit" class="fa fa-spinner fa-pulse"></i> Contact me</button>
                        <!--<input type="submit" value="Contact me">-->

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div id="footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Development Office</h4>
                    1st Floor, N3-N4 Building, Group D<br />
                    Lane 57, Lang Ha Street, Hanoi,<br />
                    Vietnam
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Representative Office</h4>
                    Spinderihallerne<br/>
                    Spinderigade 11E, Studio G14<br/>
                    7100 Vejle <br/>
                    Denmark
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Creative Office</h4>
                    Manila Rivercity - Sta. Ana<br />
                    Rhine building 21th floor, 2111<br />
                    Metro Manila
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER BOTTOM -->
<div id="footer-bottom">
    <div class="container-12">
        <div class="group">
            <div class="inner">
                <ul id="menu-footer" class="x-nav">
                    <li><a rel="m_PageScroll2id" href="#rent-developer" >Rent a developer</a></li>
                    <li><a rel="m_PageScroll2id" href="#skill-list" >Pricing</a></li>
                    <li><a rel="m_PageScroll2id" href="#flexibility-team" >Services</a></li>
                    <li><a rel="m_PageScroll2id" href="#qualification">Qualifications</a></li>
                    <li><a rel="m_PageScroll2id" href="#meet-the-team">About Us</a></li>
                    <li><a rel="m_PageScroll2id" href="#contact-form">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <p>Copyright &copy; <?php echo date('Y'); ?> TeuSoft.dk. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div style="" class="x-content-dock left x-content-dock-on" id="widget_dock_wrapper">
    <div class="widget " id="mltwtext-2"><h4 class="h-widget">Hey, Made You Look!</h4>
        <div class="MLTW_content Dinhkk-come-here"><div class="textwidget"><p>
                    It’s free to get more information! Get our white paper with prices in your mailbox.
                </p>
            </div>
            <div lang="en-US" dir="ltr" id="" class="">
                <div class="screen-reader-response"></div>
                <form id="dock_contact_form" action="#" name="">
                    <p>
                        <span class="your-email">
                            <input type="email" class="" placeholder="My Email" size="40" value="" name="email" />
                            <input type="hidden" class="" size="40" value="dock" name="form_type" />
                            <input type="hidden" class="" size="40" value="en" name="lang" />
                        </span></p>
                    <p id="message_return_dock"></p>
                    <p><button class="btn" rel="m_PageScroll2id"  onclick="submit_form_dock(); return false;"><i id="loading_submit_dock" class="fa fa-spinner fa-pulse"></i> Order Now </button></p>
                </form>
            </div>
        </div>
    </div>
    <a id="close_dock_widget" class="x-close-content-dock" href="#">
        <span>✖</span>
    </a>
</div>



<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-61637334-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    //we need email function
    function processForm()
    {
        jQuery('#loading_submit').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#contact_form').serialize(),
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            jQuery('#loading_submit').css('display','none');
            jQuery('#message_return').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return').fadeOut()", 6000);
            if (data.error === 0) {
                jQuery('#success_message').show();
                jQuery('#contact_form')[0].reset();
                jQuery('#message_return').addClass('success').removeClass('warning');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return').addClass('warning').removeClass('success');
            }
        });

        return false;
    }

    function submit_form_dock(){
        jQuery('#loading_submit_dock').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#dock_contact_form').serialize(),
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            jQuery('#loading_submit_dock').css('display','none');
            jQuery('#message_return_dock').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return_dock').fadeOut()", 6000);
            if (data.error === 0) {
                //jQuery('#success_message').show();
                jQuery('#dock_contact_form')[0].reset();
                jQuery('#message_return_dock').addClass('success').removeClass('warning');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return_dock').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return_dock').addClass('warning').removeClass('success');
            }
        });

        return false;
    }
</script>
<?php include('tawk_live_chat.php'); ?>
</body>
</html>