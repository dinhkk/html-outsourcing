<!--
    #################################
        - THEMEPUNCH BANNER -
    #################################
    -->
    <div class="tp-banner-container">
        <video autoplay  poster="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/polina.jpg" id="bgvid" loop>
            <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
            <source src="video/MVI_8431.webm" type="video/webm">
            <source src="video/MVI_8431.MP4" type="video/mp4">
        </video>
        <style>
            video{
                position: absolute;
                right: 0;
                bottom: 0;
                min-width: 100%;
                min-height: 100%;
                width: auto;
                height: auto;
                z-index: -100;
            }
        </style>
        <script>
            /*jQuery(function(){
                var BV = new jQuery.BigVideo(); BV.init();
                if ( Modernizr.touch ) {
                    BV.show('http://theme.co/x/demo/integrity/1/wp-content/uploads/sites/2/2014/06/x-home-5-bg-video-slider-touch-2.jpg');
                } else {
                    BV.show('http://192.168.1.131/html-outsourcing/video/MVI_8431.mp4', { ambient : true });
                }
            });*/

            /*jQuery(function() {
                var BV = new jQuery.BigVideo();
                BV.init();
                BV.show('http://vjs.zencdn.net/v/oceans.mp4');
            });*/
        </script>
        <div class="tp-banner">
            <ul>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off" >
                    <!-- MAIN IMAGE -->
                    <img src="http://theme.co/x/demo/integrity/1/wp-content/plugins/revslider/images/transparent.png"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->


                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption video_designs_design lfb"
                         data-x="8"
                         data-y="349"
                         data-speed="3000"
                         data-start="3000"
                         data-easing="easeOutExpo"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="300"

                         style="z-index: 8;"><img src="http://theme.co/x/demo/integrity/1/wp-content/uploads/sites/2/2013/08/design-integrity-290.jpg" alt="">
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption video_designs_design lfb"
                         data-x="306"
                         data-y="349"
                         data-speed="3000"
                         data-start="3250"
                         data-easing="easeOutExpo"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="300"

                         style="z-index: 9;"><img src="http://theme.co/x/demo/integrity/1/wp-content/uploads/sites/2/2013/08/design-renew-290.jpg" alt="">
                    </div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption video_designs_design lfb"
                         data-x="602"
                         data-y="349"
                         data-speed="3000"
                         data-start="3500"
                         data-easing="easeOutExpo"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="300"

                         style="z-index: 10;"><img src="http://theme.co/x/demo/integrity/1/wp-content/uploads/sites/2/2013/08/design-icon-290.jpg" alt="">
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption video_designs_design lfb"
                         data-x="901"
                         data-y="349"
                         data-speed="3000"
                         data-start="3750"
                         data-easing="easeOutExpo"
                         data-elementdelay="0.1"
                         data-endelementdelay="0.1"
                         data-endspeed="300"

                         style="z-index: 11;"><img src="http://theme.co/x/demo/integrity/1/wp-content/uploads/sites/2/2014/05/design-ethos-290.jpg" alt="">
                    </div>
                </li>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>

    <!-- THE SCRIPT INITIALISATION -->
    <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
    <script type="text/javascript">

        var revapi;

        jQuery(document).ready(function() {

            revapi = jQuery('.tp-banner').revolution(
                {
                    delay:9000,
                    startwidth:1170,
                    startheight:500,
                    hideThumbs:10,
                    fullWidth:"off",
                    fullScreen:"on",
                    fullScreenOffsetContainer: ""

                });

        });	//ready

    </script>

    <!-- END REVOLUTION SLIDER -->


