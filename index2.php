<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <title>HTML - TEUSOFT OUTSOURCING</title>
    <meta name="description" content="">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/1200px_12_columns_30px_gutter.css">
    <link rel="stylesheet" href="css/style.css">

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />



    <script src="./js/jquery.malihu.PageScroll2id.min.js"></script>
    <script>
        $(document).ready(function(){
            //Get the menu fixed on top

            $(window).scroll(function () {
                if ($(this).scrollTop() > $('#header').height()) {
                    $('#top').addClass("fixed-nav");
                    $('body').addClass("fixed-body");
                } else {
                    $('#top').removeClass("fixed-nav");
                    $('body').removeClass("fixed-body");
                }
            });

            //Trigger the menu
            $('#nav-trigger').click(function(){
                $('#nav ul').slideToggle('fast');
            });
        });
    </script>

</head>
<body>
<!-- Slideshow-->
<div id="header">
    <?php include('slider.php'); ?>
</div>

<!-- Global Navigation, including logo -->
<div id="top">
    <div class="container-12">
        <div class="group">
            <div class="grid-12" id="nav">
                <div id="logo"><a href="#">TeuSoft Outsourcing</a></div>
                <a id="nav-trigger" href="javascript:;"><img alt="=" src="images/menu.svg"></a>
                <ul>
                    <li><a href="#rentdeveloper" rel='m_PageScroll2id'>Rent a developer</a></li>
                    <li class="selected"><a href="#pricing" rel='m_PageScroll2id'>Pricing</a></li>
                    <li><a href="#services" rel='m_PageScroll2id'>Services</a></li>
                    <li><a href="#about_us" rel='m_PageScroll2id'>About Us</a></li>
                    <li><a href="#qualifications" rel='m_PageScroll2id'>Qualifications</a></li>
                    <li><a href="#contact_us" rel='m_PageScroll2id'>Contact Us</a></li>
                    <li id="flag">
                        <a id="current-flag" href="javascript:;"><img alt="English" src="images/gb_16x16.png"></a>
                        <div>
                            <a href="#"><img alt="Danish" src="images/dk_16x16.png"></a>
                            <a href="#"><img alt="English" src="images/gb_16x16.png"></a>
                            <a href="#"><img alt="Italian" src="images/it_16x16.png"></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- MAIN HOME PAGE -->
<div id="home" class="section">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">One out of 5 companies in Europe are outsourcing! <strong>Are you?</strong></h2>
                    <p><strong>European quality.</strong> Our developers are educated before they are hired
                        by us, and we are still coaching them with European trainers to develop their efficiency and
                        workflow. We only select the best talents so we can assure you the highest quality.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER TITLE -->
<div id="rent-developer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><h2 class="sub-head-line">We rent out developers and programmers</h2></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER CONTENT -->
<div id="rent-a-developer">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-desktop fa-fw fa-2x"></i></span>
                        <p>Development of web applications, websites & webshops.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-mobile fa-fw fa-2x"></i></span>
                        <p>Apps for smartphones & tablets.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-code fa-fw fa-2x"></i></span>
                        <p>Hardware Programming.</p>
                    </div>
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-globe fa-fw fa-2x"></i></span>
                        <p>Easy and cheap workforce with European quality.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-money fa-fw fa-2x"></i></span>
                        <p>Fixed transparent pricing with all inclusive.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-group fa-fw fa-2x"></i></span>
                        <p>Access to use other developers in our team.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-12 margin50">
                <div class="inner centerize">
                    <a class="btn" href="#contact_us" rel='m_PageScroll2id'>Contact Us</a>
                </div>
            </div>
            <div class="grid-12"><img id="img-competency" alt="" src="images/competency.png"></div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER FOOTER -->
<div id="rent-developer-footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><p>We have been doing this for a long time! Our developers will work
                        in the systems you and your company are using and we have experts in webshops, SEO & API’s to
                        Wordpress & Magento but also in administrative systems for research and project handling.</p></div>
            </div>
        </div>
    </div>
</div>





<div id="content">
    <div class="container-12">




        <div class="group section" id="pricing">
            <div class="grid-12">
                <h2 class="head-line">Rent a full time developer for under 14€ per hour</h2>
                <h2 class="sub-head-line">Combine your own package after your needs with the possibility for part-time employment.</h2>
            </div>
            <div class="group">
                <div class="grid-3">
                    <div class="inner">
                        <ul>
                            <li class="head-table">Pay as you go</li>
                            <li>40 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Including Team Leader</li>
                            <li>1 hours communication support Extra for 55€ / hour</li>
                            <li>Up to 40 unused hour can be transferred to next month</li>
                            <li>Extra hours for 25€ Overtime for 30€</li>
                            <li>Min. 3 month booking</li>
                            <li>6.900kr / måned = 172,5kr / time  <span>929€ / month = 23,25€ / hour</span></li>
                            <li>Book 6 months & get 20 hours free Book 12 months & get 40 hours free</li>
                            <li class="button">Book now</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-3">
                    <div class="inner">
                        <ul>
                            <li class="head-table">Halftime</li>
                            <li>80 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Including Team Leader</li>
                            <li>2 hours communication support Extra for 55€ / hour</li>
                            <li>Up to 40 unused hour can be transferred to next month</li>
                            <li>Extra hours for 16€ Overtime for 23€</li>
                            <li>Min. 3 month booking</li>
                            <li>10.900 / måned = 136,25kr / time <span>1459€ / month = 15,75€ / hour</span></li>
                            <li>Book 6 months & get 40 hours free Book 12 months & get 80 hours free</li>
                            <li class="button">Book now</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-3">
                    <div class="inner">
                        <ul>
                            <li class="head-table">Fulltime</li>
                            <li>160 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Including Team Leader</li>
                            <li>4 hours communication support Extra for 55€ / hour</li>
                            <li>Up to 80 unused hour can be transferred to next month</li>
                            <li>Extra hours for 14€ Overtime for 18€</li>
                            <li>Min. 3 month booking <strong>Get 40 hours free</strong></li>
                            <li>15.900kr / måned = 99,50kr / time <span>2129€ / month = 13,30€ / hour</span></li>
                            <li>Book 6 months & get 80 hours free Book 12 months & get 160 hours free</li>
                            <li class="button">Book now</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-3">
                    <div class="inner">
                        <ul>
                            <li class="head-table">Fulltime</li>
                            <li>160 hours a month</li>
                            <li>Dedicated developer</li>
                            <li>Including Team Leader</li>
                            <li>Special quote for you needs</li>
                            <li>Up to 80 hours per developer can be transferred to next month</li>
                            <li>Special quote for you needs</li>
                            <li>Min. 3 month booking</li>
                            <li>Special quote for you needs</li>
                            <li>Special discount for you needs</li>
                            <li class="button">Contact for quote</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid-12">
                <div class="inner">
                    (icon) Flexibility is our keyword! When you hire a developer through us, you
                    also get access to the whole team. If you suddenly get a good idea or you are in need of special
                    support, you can get help from other developers with another skill set. So you do not need to hire
                    more people, find a freelancer or buy extra support. You get the full package.
                </div>
            </div>
        </div>

        <div class="group section" id="services">
            <div class="grid-12">
                <h2 class="head-line">Always English support with Danish quality and project management.</h2>
            </div>
            <div class="group">
                <div class="grid-4">
                    <div class="inner">
                        <ul>
                            <li>Your own dedicated developer for a fixed price per month.</li>
                            <li>Use as your own employee.</li>
                            <li>You decide the qualifications of your worker.</li>
                            <li class="button">Learn more</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-4">
                    <div class="inner">
                        <ul>
                            <li>Possibility for a 14 days trial for a low fixed price.</li>
                            <li>Use the person when you have work.</li>
                            <li>Fixed low price, no extra fees</li>
                            <li class="button">Learn more</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-4">
                    <div class="inner">
                        <ul>
                            <li>Use us as an office</li>
                            <li>We take care of your project from start to end</li>
                            <li>Projects for a fixed time and price</li>
                            <li class="button">Get FREE Quote</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="group section" id="qualifications">
            <div class="grid-12">
                <h2 class="head-line">We’re good with techs</h2>
                <h2 class="sub-head-line">And we only use the newest technologies and workflow</h2>
            </div>
            <div class="group">
                <div class="grid-6">
                    <div class="inner">
                        <ul>
                            <li>Responsive design</li>
                            <li>HTML5 & CSS3</li>
                            <li>Javascript & jQuery</li>
                            <li>API's</li>
                            <li>OOP & (H)MVC</li>
                            <li>Git/SVN</li>
                            <li>Lots of frameworks</li>
                            <li>Tons of administrative systems</li>
                        </ul>
                    </div>
                </div>
                <div class="grid-6">
                    <div class="inner">
                        <ul>
                            <li>Frontend, SEO & graphic design</li>
                            <li>iOS development</li>
                            <li>Android development</li>
                            <li>PHP</li>
                            <li>.NET & C#</li>
                            <li>Node.js</li>
                            <li>Python</li>
                            <li>And lots more</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid-12">
                <div class="inner">
                    (icon) Communication is our most important service! We are always in
                    close dialogue with you as our client. We have tight deadlines and we will also work overtime to get
                    there. You decide how much support you need. We will be ready at the phone and email.
                </div>
            </div>
        </div>

        <div class="group section" id="about_us">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Meet the team</h2>
                    <h2 class="sub-head-line">Since 2005, we have been working with webshops, Apps, software and web
                        development for European companies.</h2>
                </div>
            </div>
        </div>

        <div class="group section" id="contact_us">
            <div class="grid-12">
                <h2 class="head-line">Want to know more? – Contact me.</h2>
            </div>
            <div class="group">
                <div class="grid-6">
                    <div class="inner">
                        <form id="form_contact_us" name="form_contact_us">
                            <ul>
                                <li><label for="cf_my_name">My Name (text-field - optional)</label></li>
                                <li><input id="cf_my_name" value="" /></li>
                                <li><label for="cf_my_email">My Email (text-field - *required)</label></li>
                                <li><input id="cf_my_email" value="" /></li>
                                <li><label for="cf_subject">Subject</label></li>
                                <li>
                                    <select id="cf_subject" name="cf_subject">
                                        <option value="Order info">Order info</option>
                                        <option value="Order info">Rent developer</option>
                                        <option value="Order info">Free quote</option>
                                        <option value="Order info">Other</option>
                                    </select>
                                </li>
                                <li><label for="cf_message">Your Message</label></li>
                                <li>
                                    <textarea name="cf_message" id="cf_message"></textarea>
                                </li>
                                <li><input type="button" id="cf_submit" value="Contact me" /></li>
                            </ul>
                        </form>
                    </div>
                </div>
                <div class="grid-6">
                    <div class="inner">image of me here</div>
                    <div class="inner">It’s me you will be in contact with and I am really looking forward to talk to you :)</div>
                    <div class="inner">
                        <ul>
                            <li>Bjarke B Rubeksen</li>
                            <li>+45 65 74 04 74 [Denmark]</li>
                            <li>+84 123 524 6008 [Vietnam]</li>
                            <li>Mail : bjarke@teusoft.dk</li>
                            <li>Skype ikon : mydunlet</li>
                            <li>Facebook ikon : https://facebook.com/bjarker</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('contact-us.php'); ?>
<?php include('footer.php'); ?>
</body>
</html>
