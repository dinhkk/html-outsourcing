<!DOCTYPE html>
<html>
<head lang="en">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <!--<BASE href="http://localhost/html-outsourcing/">-->
    <BASE href="http://outsourcing.teusoft.dk/">
    <title>Vi udlejer fleksible webudvikler & programmører</title>
    <meta name="description" content="Lej din egen faste udvikler eller programmør med dansk support i Vietnam">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/1200px_12_columns_30px_gutter.css">
    <link rel="stylesheet" href="css/style.css">


    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <script src="js/jquery-2.1.3.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    <script src="./js/jquery.malihu.PageScroll2id.min.js"></script>
    <script src="./js/script.js"></script>
    <!--<script src="//vjs.zencdn.net/4.3/video.js"></script>
    <script src="./js/bigvideo.js"></script>-->
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

</head>
<body>
<!-- Slideshow-->
<div id="header">
    <?php include('slider.php'); ?>
</div>

<!-- Global Navigation, including logo -->
<div id="top">
    <div class="container-12">
        <div class="group">
            <div class="grid-12" id="nav">
                <div id="logo"><img alt="" src="images/teu-logo-01.svg" ><a href="./da/#">TeuSoft Outsourcing</a></div>
                <a id="nav-trigger" href="javascript:;"><img alt="=" src="images/menu.svg"></a>
                <ul>
                    <li><a href="./da/#rent-developer" rel='m_PageScroll2id'>Lej webudvikler</a></li>
                    <li class="selected"><a href="./da/#skill-list" rel='m_PageScroll2id'>Priser</a></li>
                    <li><a href="./da/#flexibility-team" rel='m_PageScroll2id'>Services</a></li>
                    <li><a href="./da/#qualification" rel='m_PageScroll2id'>Kompetencer</a></li>
                    <li><a href="./da/#about_us" rel='m_PageScroll2id'>Om os</a></li>
                    <li><a href="./da/#contact-form" rel='m_PageScroll2id'>Kontakt</a></li>
                    <li id="flag">
                        <a id="current-flag" href="javascript:;"><img alt="English" src="images/gb_16x16.png"></a>
                        <div>
                            <a href="./da/"><img alt="Danish" src="images/dk_16x16.png"></a>
                            <a href="./"><img alt="English" src="images/gb_16x16.png"></a>
                            <a href="./it/"><img alt="Italian" src="images/it_16x16.png"></a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- MAIN HOME PAGE -->
<div id="home">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Hver 5 danske virksomhed outsourcer til udlandet! <strong>Gør du?</strong></h2>
                    <p><strong>Dansk kvalitet.</strong> Vores udviklere uddannet før de kommer til os, men vi har løbende danskere der træner,
                        coacher og udvikler dem, så de arbejder endnu mere effektivt. Ligeledes håndplukker vi kun de bedste talenter ud så vi er sikre på en høj kvalitet.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER TITLE -->
<div id="rent-developer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><h2 class="sub-head-line">Vi udlejer webudviklere og programører</h2></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER CONTENT -->
<div id="rent-a-developer">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-desktop fa-fw fa-2x"></i></span>
                        <p>Udvikling af web-applikationer, hjemmesider og webshops.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-mobile fa-fw fa-2x"></i></span>
                        <p>Apps til smartphones & tablets.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-code fa-fw fa-2x"></i></span>
                        <p>Hardware programmering.</p>
                    </div>
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <div class="row">
                        <span class="icon-box"><i class="fa fa-globe fa-fw fa-2x"></i></span>
                        <p>Nem og billig arbejdskraft med dansk kvalitet.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-money fa-fw fa-2x"></i></span>
                        <p>Faste priser med alt inkl.</p>
                    </div>

                    <div class="row">
                        <span class="icon-box"><i class="fa fa-group fa-fw fa-2x"></i></span>
                        <p>Adgang til brug af andre personer i vores afdeling.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-12 margin50">
                <div class="inner centerize">
                    <a class="btn" rel="m_PageScroll2id" href="./da/#contact-form" >Bestil info brochure</a>
                </div>
            </div>
            <div class="grid-12"><img id="img-competency" alt="" src="images/competency.png"></div>
        </div>
    </div>
</div>

<!-- RENT A DEVELOPER FOOTER -->
<div id="rent-developer-footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner"><p>Vi har mange år på bagen! Vores udviklere arbejder i de systemer
                        som du og din virksomhed er vandt til at bruge og vi har eksperter i alt fra opsætning af
                        webshops, SEO & API’er, Wordpress & Magento men også i de administrative systemer som du
                        benytter til planlægning og research.</p></div>
            </div>
        </div>
    </div>
</div>

<!-- RENT FULL TIME DEVELOPER WITH IMAGE -->
<div id="rent-fulltime-developer-img" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Lej en fuldtids webudvikler for under<br><span>100dkk i timen</span></h2>
                    <h2 class="sub-head-line">Sammensæt selv din pakke efter dine behov med mulighed for deltid ansættelse.</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- LIST SKILLS -->
<div id="skill-list">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <div class="col">
                        <ul>
                            <li class="head-table">Pay as you go</li>
                            <li class="price-cell">172,5kr<span>time</span></li>
                            <li>40 timer per måned</li>
                            <li>Dedikeret udvikler</li>
                            <li>Inkl. Team Leader</li>
                            <li>1 times support*</li>
                            <li>Overfør 40 timer**</li>
                            <li>Extra hours for 180kr<br>Overtid for 225kr</li>
                            <li>3 måneders binding***</li>
                            <li>6 gir dig 20 gratis timer<br>12 gir dig 40 gratis timer<br>&nbsp;</li>
                            <li>6.900kr / måned</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Book nu</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Halftime</li>
                            <li class="price-cell">136,25kr<span>time</span></li>
                            <li>80 timer per måned</li>
                            <li>Dedikeret udvikler</li>
                            <li>Inkl. Team Leader</li>
                            <li>2 timers support*</li>
                            <li>Overfør 40 timer**</li>
                            <li>Extra hours for 120kr<br>Overtid for 170kr</li>
                            <li>3 måneders binding***</li>
                            <li>6 gir dig 40 gratis timer <br/>12 gir dig 80 gratis timer<br>&nbsp;</li>
                            <li>10.900 / måned</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Book nu</a></li>
                        </ul>
                    </div>
                    <div class="col featured">
                        <ul>
                            <li class="head-table">Fulltime</li>
                            <li class="price-cell">99,50kr<span>time</span></li>
                            <li>160 timer per måned</li>
                            <li>Dedikeret udvikler</li>
                            <li>Inkl. Team Leader</li>
                            <li>4 timers support*</li>
                            <li>Overfør 80 timer**</li>
                            <li>Extra hours for 99,50kr<br>Overtid for 135kr</li>
                            <li>3 måneders binding***</li>
                            <li><strong>Få 40 timer gratis</strong><br>6 gir dig 80 gratis timer<br>12 gir dig 160 gratis timer</li>
                            <li>15.900kr / måned</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Book nu</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li class="head-table">Special</li>
                            <li class="price-cell">Speciel<span>&nbsp;</span></li>
                            <li>Dit behov</li>
                            <li>Dedikeret udvikler</li>
                            <li>Inkl. Team Leader</li>
                            <li>Dit behov</li>
                            <li>Overfør 80 timer <span class="small">/udvikler**</span></li>
                            <li>Specielt tilbud for dine behov<br>&nbsp;</li>
                            <li>3 måneders binding***</li>
                            <li>Specielt tilbud<br>&nbsp;<br>&nbsp;</li>
                            <li>Speciel</li>
                            <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Kontakt for tilbud</a></li>
                        </ul>
                    </div>
                </div>
                <div class="inner explanation">
                    <p>*  Avanceret support i dit eget sprog. Yderligere support kan tilkøbes for 450 kr pr. time.</p>
                    <p>** Ubrugte udviklingstimer kan overføres til næste måned.</p>
                    <p>*** Minimum 3 måneders booking. Opsigelse med 2 måneders varsel.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Flexibility team -->
<div id="flexibility-team" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Fleksibilitet er vores nøgleord!</h2>
                    <p> Når du ansætter en fast udvikler
                        igennem os, får du også adgang til hele teamet. Hvis du lige pludselig står med en ny god idé eller
                        akut har brug for en support, kan du få hjælp fra en af de andre udviklere med andre kvalifikationer
                        i vores team. Du behøver derfor ikke ansætte flere, finde en freelancer eller tilkøbe anden support.
                        Du får hele pakken.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- SERVICES -->
<div id="services">
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <h2 class="head-line">Altid dansk support, kvalitet og projektledelse.</h2>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-pie-chart fa-2x"></i>
                    <ul>
                        <li>Din egen dedikerede udvikler til en fast pris pr. måned.</li>
                        <li>Fungerer som din egen medarbejdere.</li>
                        <li>Bestem selv kvalifikationer for medarbejder</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Hør mere</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-area-chart fa-2x"></i>
                    <ul>
                        <li>Mulighed for 14 dage’s prøveperiode til fast lav pris.</li>
                        <li>Brug personen som du vil, når du har lyst.</li>
                        <li>Fast timepris, ingen gebyrer.</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Hør mere</a></li>
                    </ul>
                </div>
            </div>
            <div class="grid-4 margin50">
                <div class="inner">
                    <i class="fa fa-line-chart fa-2x"></i>
                    <ul>
                        <li>Brug os som bureau</li>
                        <li>Vi ordner hele projektet fra start til slut</li>
                        <li>Projekt til fast aftalt tid og pris <br>&nbsp;</li>
                        <li class="button"><a class="btn" rel="m_PageScroll2id" href="./da/#contact-form">Få et GRATIS tilbud</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- QUALIFICATION -->
<div id="qualification" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Vi er gode med technologier</h2>
                    <p>Og vi arbejder kun med de nyeste teknologier og arbejdsmetoder.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="technology">
    <div class="container-12">
        <div class="group">
            <div class="grid-5 offset-1">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-arrows-h"></i>Responsivt design</li>
                        <li><i class="fa fa-html5"></i>HTML5 & CSS3</li>
                        <li><i class="fa fa-connectdevelop"></i>Javascript & jQuery</li>
                        <li><i class="fa fa-code"></i>API’er</li>
                        <li><i class="fa fa-bug"></i>OOP & (H)MVC</li>
                        <li><i class="fa fa-git"></i>Git/SVN</li>
                        <li><i class="fa fa-puzzle-piece"></i>Masser af frameworks</li>
                        <li><i class="fa fa-server"></i>Og tonsvis af administrative systemer</li>
                    </ul>
                </div>
            </div>
            <div class="grid-5">
                <div class="inner">
                    <ul>
                        <li><i class="fa fa-picture-o"></i>Frontend, SEO & grafisk design</li>
                        <li><i class="fa fa-apple"></i>iOS udvikling</li>
                        <li><i class="fa fa-android"></i>Android udvikling</li>
                        <li><i class="fa fa-check"></i>PHP</li>
                        <li><i class="fa fa-wrench"></i>.NET & C#</li>
                        <li><i class="fa fa-bolt"></i>Node.js</li>
                        <li><i class="fa fa-terminal"></i>Python</li>
                        <li><i class="fa fa-codepen"></i>Og meget mere.</li>
                    </ul>
                </div>
            </div>
            <div class="grid-12 centerize margin50" id="about_us">
                <div class="inner">
                    <h2 class="head-line">Kommunikation er vores vigtigste service!</h2>
                    <p>Derfor er vi altid i tæt dialog med dig. Vi har stramme deadlines og arbejder gerne over tid. Vi leverer alt den support
                        som du har brug for og jeg sidder altid klar med dansk support ved både mail og telefon.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- MEET THE TEAM -->
<div id="meet-the-team" class="centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Mød personerne bag</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- EXPERT -->
<div id="expert">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <img alt="expert team" src="images/expert.png">
                </div>
            </div>
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Mød experterne</h2>
                    <p>Vi har udvalgt de bedste senior udviklere, forretningsudviklere og project managers til at dele deres viden og bygge verdens bedste online produkter. Kommunikation, fleksibilitet og tillid er vores nøgleord.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="meet-the-team-desc" class="white-row centerize">
    <div class="container-12">
        <div class="group">
            <div class="grid-12">
                <div class="inner">
                    <h2 class="head-line">Vi har eksisteret siden 2005</h2>
                    <p>og har siden arbejdet med e-handel, udvikling,
                        apps for danske og Europæiske virksomheder.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT BJARKE -->
<div id="contact-us">
    <div class="container-12">
        <div class="group">
            <div class="grid-7">
                <div class="inner">
                    <h2>Det er mig som du kommer i kontakt med og du kan tro at jeg glæder mig til at snakke med dig <i class="fa fa-smile-o"></i></h2>
                    <ul>
                        <li><i class="fa fa-user"></i>Bjarke B Rubeksen</li>
                        <li><i class="fa fa-phone"></i>+45 65 74 04 74 [Denmark]</li>
                        <li><i class="fa fa-phone"></i>+84 123 524 6008 [Vietnam]</li>
                        <li><i class="fa fa-envelope"></i><a href="mailto:bjarke@teusoft.dk">bjarke@teusoft.dk</a></li>
                        <li><i class="fa fa-skype"></i>mydunlet</li>
                        <li><i class="fa fa-facebook-official"></i><a href="https://facebook.com/bjarker">facebook.com/bjarker</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CONTACT FORM -->
<div id="contact-form">
    <div class="container-12">
        <div class="group">
            <div class="grid-6">
                <div class="inner">
                    <h2 class="head-line">Vil du vide mere?<br>Kontakt mig.</h2>
                    <p>Så du er nået til bunden og jeg håber du er blevet interesseret. Kontakt os at få:</p>
                    <ul>
                        <li><i class="fa fa-angle-double-right"></i>Vores white-paper sendt direkte til din mail</li>
                        <li><i class="fa fa-angle-double-right"></i><strong>Rabat</strong> på en dedikeret udvikler</li>
                        <li><i class="fa fa-angle-double-right"></i><strong>Gratis</strong> tilbud på dit næste projekt.</li>
                        <li><i class="fa fa-angle-double-right"></i>Det koster dig ikke noget!</li>
                    </ul>
                </div>
            </div>
            <div class="grid-6 margin50">
                <div class="inner">
                    <form id="contact_form" action="" >
                        <input name="name" type="text" placeholder="Mit Navn (optional)">
                        <input name="email" type="text" placeholder="Min Email (required)">
                        <input type="hidden" name="action" value="do">
                        <input type="hidden" class="" size="40" value="da" name="lang" />
                        <select name="subject">
                            <option selected>Emne (optional)</option>
                            <option>Bestil info</option>
                            <option>Lej udvikler</option>
                            <option>Bestil tilbud</option>
                            <option>Andet</option>
                        </select>
                        <textarea name="message" placeholder="Din Besked"></textarea>
                        <p id="message_return"></p>
                        <button onclick="processForm();return false"><i id="loading_submit" class="fa fa-spinner fa-pulse"></i> Kontakt mig</button>
                        <!--<input type="submit" value="Contact me">-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<div id="footer" class="white-row">
    <div class="container-12">
        <div class="group">
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Development Office</h4>
                    1st Floor, N3-N4 Building, Group D<br />
                    Lane 57, Lang Ha Street, Hanoi,<br />
                    Vietnam
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Representative Office</h4>
                    Spinderihallerne<br/>
                    Spinderigade 11E, Studio G14<br/>
                    7100 Vejle <br/>
                    Denmark
                </div>
            </div>
            <div class="grid-4">
                <div class="inner">
                    <h4><i class="fa fa-chevron-right"></i> Creative Office</h4>
                    Manila Rivercity - Sta. Ana<br />
                    Rhine building 21th floor, 2111<br />
                    Metro Manila
                </div>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER BOTTOM -->
<div id="footer-bottom">
    <div class="container-12">
        <div class="group">
            <div class="inner">
                <ul id="menu-footer" class="x-nav">
                    <li><a rel="m_PageScroll2id" href="./da/#rent-developer" >Lej webudvikler</a></li>
                    <li><a rel="m_PageScroll2id" href="./da/#skill-list" >Priser</a></li>
                    <li><a rel="m_PageScroll2id" href="./da/#flexibility-team" >Services</a></li>
                    <li><a rel="m_PageScroll2id" href="./da/#qualification">Kompetencer</a></li>
                    <li><a rel="m_PageScroll2id" href="./da/#meet-the-team">Om os</a></li>
                    <li><a rel="m_PageScroll2id" href="./da/#contact-form">Kontakt</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-12">
        <div class="group">
            <div class="grid-12 centerize">
                <div class="inner">
                    <p>Copyright &copy; <?php echo date('Y'); ?> TeuSoft.dk. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</div>


<div style="" class="x-content-dock left x-content-dock-on" id="widget_dock_wrapper">
    <div class="widget " id="mltwtext-2"><h4 class="h-widget">Hey, Made You Look!</h4>
        <div class="MLTW_content Dinhkk-come-here"><div class="textwidget"><p>
                    Det er gratis at hører mere! Få vores info brochure med priser på mail.
                </p>
            </div>
            <div lang="en-US" dir="ltr" id="" class="">
                <div class="screen-reader-response"></div>
                <form id="dock_contact_form" action="#" name="">
                    <p>
                        <span class="your-email">
                            <input type="email" class="" placeholder="Min Email" size="40" value="" name="email" />
                            <input type="hidden" class="" size="40" value="da" name="lang" />
                            <input type="hidden" class="" size="40" value="dock" name="form_type" />
                        </span></p>
                    <p id="message_return_dock"></p>
                    <p><button class="btn" rel="m_PageScroll2id"  onclick="submit_form_dock(); return false;"><i id="loading_submit_dock" class="fa fa-spinner fa-pulse"></i> Bestil Nu </button></p>
                </form>
            </div>
        </div>
    </div>
    <a id="close_dock_widget" class="x-close-content-dock" href="#">
        <span>✖</span>
    </a>
</div>



<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-61637334-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();


    //we need email function
    function processForm()
    {
        jQuery('#loading_submit').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#contact_form').serialize(),
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            jQuery('#loading_submit').css('display','none');
            jQuery('#message_return').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return').fadeOut()", 6000);
            if (data.error === 0) {
                jQuery('#success_message').show();
                jQuery('#contact_form')[0].reset();
                jQuery('#message_return').addClass('success').removeClass('warning');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return').addClass('warning').removeClass('success');
            }
        });

        return false;
    }

    function submit_form_dock(){
        jQuery('#loading_submit_dock').css('display','inherit');
        jQuery.ajax({
            url: 'sendmail2.php',
            type: 'POST',
            data: jQuery('#dock_contact_form').serialize(),
            dataType: 'json'
        }).done(function(data) {
            console.log(data);
            jQuery('#loading_submit_dock').css('display','none');
            jQuery('#message_return_dock').html(data.message).css('display','block');

            setTimeout("jQuery('#message_return_dock').fadeOut()", 6000);
            if (data.error === 0) {
                //jQuery('#success_message').show();
                jQuery('#dock_contact_form')[0].reset();
                jQuery('#message_return_dock').addClass('success').removeClass('warning');
                //reloadCaptcha();
                //setTimeout("jQuery('#message_return').fadeOut()", 12000);
            } else {
                jQuery('#message_return_dock').append(' ' + data.error_detail).css('display','block');
                jQuery('#message_return_dock').addClass('warning').removeClass('success');
            }
        });

        return false;
    }
</script>
<?php include('tawk_live_chat.php'); ?>
</body>
</html>