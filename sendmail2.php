<?php
/**
* created by dinhkk
* current system date
* 2:36 AM 16-Apr-15 04 2015
* html-outsourcing
**/

/*header('Content-Type: application/json');
var_dump($_POST);
die();*/

require 'mailgun-php/vendor/autoload.php';
use Mailgun\Mailgun;

//if($_POST['action']=='do'): // check submit action
global $mg,$domain;

$mg = new Mailgun("key-7d26e63b4fd17d110e40c12cf9221674");
$domain = "outsourcing.teusoft.dk";


/*# Next, instantiate a Message Builder object from the SDK.
$messageBldr = $mg->MessageBuilder();
# Define the from address.
$messageBldr->setFromAddress("bjarke@teusoft.dk", array("first"=>"Bjarke", "last" => "B Rubeksen"));
# Define a to recipient.
$messageBldr->addToRecipient("dinhtt@teusoft.dk", array("first" => "John", "last" => "Doe"));
# Define a cc recipient.
$messageBldr->addCcRecipient("info@dinhkk.com", array("first" => "Sally", "last" => "Doe"));
# Define the subject.
$messageBldr->setSubject("A message from the PHP SDK using Message Builder!");
# Define the body of the message.
$messageBldr->setTextBody(phpMailer_get_template(array('name' => 'dinhkk test mailgun'),true));
# Other Optional Parameters.
$messageBldr->setClickTracking(true);*/


//we need to check form variation
$check = validateFields();

/*# Finally, send the message. */
if (sizeof($check) === 0) {
    $status = send_email_admin($mg,$domain,@$_POST);
    send_email_customer($mg,$domain,@$_POST);

    if($status != 200) {
        $data = array(
            'message' => 'Message could not be sent.',
            'error' => 1
        );
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }else{
        $data = array(
            'message' => 'Message has been sent.',
            'error' => 0
        );
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }
}else{
    $data = array(
        'message' => 'Message could not be sent.',
    );
    $data = array_merge($data,$check);
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}


/**
 * Retrieve a template file.
 *
 * @param string $path
 * @param mixed $var
 * @param bool $return
 * @return void
 * @since 1.0.0
 */
function phpMailer_get_template($template,$var,$return=false) {
    $template = __DIR__.'/phpMailer/'.$template;

    if ( $var && is_array( $var ) )
        extract( $var );

    if( $return )
    { ob_start(); }

    // include file located
    include( $template );

    if( $return )
    { return ob_get_clean(); }
}

function validateFields(){
    $errors = array();
    $name    = @$_POST['name'];    // name from the form
    $email   = @$_POST['email'];   // email from the form
    $message = @$_POST['message']; // the message from the form
    $captcha = @$_POST['captcha']; // the user's entry for the captcha code
    $name    = substr($name, 0, 64);  // limit name to 64 characters

    /*if($name='' || empty($name) || !isset($name)) {
        $errors['error_detail'] = 'Missing name field';
        return $errors;
    }*/

    if($email='' || empty($email) || !isset($email)) {
        $errors['error_detail'] = 'Missing mail field';
        return $errors;
    }
    //$email = 'thedinh.trih@gmail.com';
    //$check  = isValidEmail(@$_POST['email']);

    if (isValidEmail(@$_POST['email']) == false) {
        $errors['error_detail'] =  'Email format is incorrect !';
        return $errors;
    }

    /*if (sizeof($errors) == 0) {
        if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&
            $_SESSION["math_code"] == $_POST["captcha"]){
            //Continue
        }else{
            $errors['captcha_error'] = 'Captcha is wrong !';
            $errors['session'] = $_SESSION["math_code"];
            $errors['post'] = $_POST["captcha"];

        }
    }
    $errors['math'] = create_new_math();*/
    return $errors;
}

function create_new_math(){
    $n1=rand(1,6);
    $n2=rand(5,9);
    $answer=$n1+$n2;
    $math = "What is ".$n1." + ".$n2." : ";
    $_SESSION['math_code'] = $answer;
    return $math;
}

function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL)
    && preg_match('/@.+\./', $email)
    && !preg_match('/@\[/', $email)
    && !preg_match('/".+@/', $email)
    && !preg_match('/=.+@/', $email);
}


function send_email_admin($mg,$domain,$data){
    $result = $mg->sendMessage($domain, array(
        'from'    => 'Bjarke <bjarke@teusoft.dk>',
        'to'      => 'bjarke@teusoft.dk',
        //'cc'      => @$_POST['email'],
        'bcc'     => 'dinhtt@teusoft.dk',
        'subject' => 'TeuSoft Outsourcing - '.@$_POST['subject'],
        'text'    => 'Testing some Mailgun awesomness!',
        'html'    => phpMailer_get_template('tpl_admin.php',@$_POST,true),
    ), array(
        //'attachment' => array('@/path/to/file.txt', '@/path/to/file.txt')
    ));

    $httpResponseCode = $result->http_response_code;

    return $httpResponseCode;
}

function send_email_customer($mg,$domain,$data){
    $content = phpMailer_get_template('tpl_customer.php',@$_POST,true);

    //we change content for Danish
    if(isset($_POST['lang']) && $_POST['lang']=='da'){
        if($_POST['form_type']=='dock'){
            $content = phpMailer_get_template('tpl_customer_da_dock.php',@$_POST,true);
        }else{
            $content = phpMailer_get_template('tpl_customer_da.php',@$_POST,true);
        }
    }

    //we need content for Italian
    if(isset($_POST['lang']) && $_POST['lang']=='it'){
        if($_POST['form_type']=='dock'){
            $content = phpMailer_get_template('tpl_customer_it_dock.php',@$_POST,true);
        }else{
            $content = phpMailer_get_template('tpl_customer_it.php',@$_POST,true);
        }
    }

    //we need content for english
    if(isset($_POST['lang']) && $_POST['lang']=='en'){
        if(isset($_POST['form_type']) && $_POST['form_type']=='dock'){
            $content = phpMailer_get_template('tpl_customer_dock.php',@$_POST,true);
        }
    }


    $result = $mg->sendMessage($domain, array(
        'from'    => 'Bjarke <bjarke@teusoft.dk>',
        'to'      => @$_POST['email'],
        //'cc'      => @$_POST['email'],
        'bcc'     => 'dinhtt@teusoft.dk',
        'subject' => 'TeuSoft Outsourcing - '.@$_POST['subject'],
        'text'    => 'Testing some Mailgun awesomness!',
        'html'    => $content,
    ), array(
        //'attachment' => array('@/path/to/file.txt', '@/path/to/file.txt')
    ));

    $httpResponseCode = $result->http_response_code;

    return $httpResponseCode;
}

//we finish check action of submit
//endif;
?>
 
