<?php
session_start(); // this MUST be called prior to any output including whitespaces and line breaks!

//if($_POST['action']=='do'): // check submit action

require 'phpMailer/PHPMailerAutoload.php';
$mail = new PHPMailer;
//$mail->SMTPDebug = 3;                               // Enable verbose debug output
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.sendgrid.net';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'dinhkk';                 // SMTP username
$mail->Password = 'thedinh87';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$array = array(
    'name' => $_POST['name'],
    'email' => $_POST['email'],
    'subject' => $_POST['subject'],
    'message' => $_POST['message'],
);

$message =  phpMailer_get_template($array,true);

$mail->From = 'bjarke@teusoft.dk';
$mail->FromName = 'Bjarke';
$mail->addAddress('dinhtt@teusoft.dk', 'TeuSoft-OutSourcing');     // Add a recipient
//$mail->addAddress('ellen@example.com');               // Name is optional
//$mail->addReplyTo('info@example.com', 'Information');
//$mail->addCC('cc@example.com');
$mail->addBCC('info@dinhkk.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name


$mail->isHTML(true);                                  // Set email format to HTML
$mail->CharSet = 'UTF-8';
$mail->Subject = 'TeuSoft OutSourcing - Subject here';
$mail->Body    = $message;
$mail->AltBody = 'test none html client';


//we need to check form variation
$check = validateFields();

if (sizeof($check) === 0) {
    if(!$mail->send()) {
        $data = array(
            'message' => 'Message could not be sent.',
            'error' => 1
        );
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }else{
        $data = array(
            'message' => 'Message has been sent.',
            'error' => 0
        );
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }
}else{
    $data = array(
        'message' => 'Message could not be sent.',
    );
    $data = array_merge($data,$check);
    header('Content-Type: application/json');
    echo json_encode($data);
    die();
}


/**
 * Retrieve a template file.
 *
 * @param string $path
 * @param mixed $var
 * @param bool $return
 * @return void
 * @since 1.0.0
 */
    function phpMailer_get_template($var,$return=false) {
        $template = '/phpMailer/basic.php';

        if ( $var && is_array( $var ) )
            extract( $var );

        if( $return )
        { ob_start(); }

        // include file located
        include( $template );

        if( $return )
        { return ob_get_clean(); }
    }

    function validateFields(){
        $errors = array();
        $name    = @$_POST['name'];    // name from the form
        $email   = @$_POST['email'];   // email from the form
        $message = @$_POST['message']; // the message from the form
        $captcha = @$_POST['captcha']; // the user's entry for the captcha code
        $name    = substr($name, 0, 64);  // limit name to 64 characters

        if($name='' || empty($name) || !isset($name)) {
            $errors['error_detail'] = 'Missing name field';
            return $errors;
        }
        if($email='' || empty($email) || !isset($email)) {
            $errors['error_detail'] = 'Missing mail field';
            return $errors;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors['error_detail'] = 'Email format is incorrect';
            return $errors;
        }

        /*if (sizeof($errors) == 0) {
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&
                $_SESSION["math_code"] == $_POST["captcha"]){
                //Continue
            }else{
                $errors['captcha_error'] = 'Captcha is wrong !';
                $errors['session'] = $_SESSION["math_code"];
                $errors['post'] = $_POST["captcha"];

            }
        }
        $errors['math'] = create_new_math();*/
        return $errors;
    }

    function create_new_math(){
        $n1=rand(1,6);
        $n2=rand(5,9);
        $answer=$n1+$n2;
        $math = "What is ".$n1." + ".$n2." : ";
        $_SESSION['math_code'] = $answer;
        return $math;
    }

    //we finish check action of submit
    //endif;
?>