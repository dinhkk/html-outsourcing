$(document).ready(function(){
    //Get the menu fixed on top

    $(window).scroll(function () {
        if ($(this).scrollTop() > $('#header').height()) {
            $('#top').addClass("fixed-nav");
            $('body').addClass("fixed-body");
        } else {
            $('#top').removeClass("fixed-nav");
            $('body').removeClass("fixed-body");
        }

    });

    $("a[rel='m_PageScroll2id']").mPageScroll2id();

    //Trigger the menu
    $('#nav-trigger').click(function(){
        $('#nav ul').slideToggle('fast');
    });

    $(window).bind('scroll',function(e) {
        var $scrolled = $(window).scrollTop();
        parallaxScroll($scrolled);
        menu_high_light();

    });


    $(function() {
        /*var $scrolled = $(window).scrollTop();
        parallaxScroll($scrolled);
        menu_high_light();*/

        //
        $(window).scroll(function(){
            var distanceTop = $('#skill-list').offset().top - $(window).height();
            if  ($(window).scrollTop() > distanceTop)
                $('#widget_dock_wrapper').animate({'left':'20px'},"fast");
            else
                $('#widget_dock_wrapper').stop(true).animate({'left':'-415px'},100);
        });

        $('#close_dock_widget').bind('click',function(){
            $(this).parent().remove();
            return false;
        });
    });


});

var menu_high_light = function(){
    $("a[rel='m_PageScroll2id']").each(function(){
        var check = $(this).hasClass('mPS2id-highlight-first');
        if(check==true){
            $(this).parent().addClass('selected DINHKK');
        }else{
            $(this).parent().removeClass('selected DINHKK');
        }
    });

}


var parallaxScroll = function($scrolled){
    var $header = $('#header').height();
    var $top = $('#top').height();
    var $home = $('#home').height();
    var $rent_developer = $('#rent-developer').height();
    var $rent_a_developer = $('#rent-a-developer').height();
    var $rent_developer_footer = $('#rent-developer-footer').height();
    var $rent_fulltime_developer_img = $('#rent-fulltime-developer-img').height();
    var $skill_list = $('#skill-list').height();
    var $flexibility_team = $('#flexibility-team').height();
    var $services = $('#services').height();
    var $qualifications = $('#qualifications').height();
    var $qualifications_2 = $('#qualifications-2').height();
    var $meet_the_team = $('#meet-the-team').height();
    var $meet_the_team_desc = $('#meet-the-team-desc').height();
    var $contact_us = $('#contact-us').height();
    var $contact_form = $('#contact-form').height();
    var $window = $(window).height();

    //define height to scroll
    var $scroll_parallax_0 = $header + $top + $home + $rent_developer + $rent_a_developer + $rent_developer_footer +
        $rent_fulltime_developer_img + $skill_list + $flexibility_team + $services + $qualifications + $qualifications_2 + $meet_the_team;

    var $meet_team_pos = $('#meet-the-team').offset().top;

    //console.log($scrolled + "--->>" + $meet_team_pos);

    if(($scrolled + 400) >=$meet_team_pos && $meet_team_pos >= $scrolled){
        var $meet_scroll_team = $meet_team_pos - $scrolled - 400;
        $('#meet-the-team').css('background-position','0 '+ ($meet_scroll_team * 0.35) +'px');
    }else{
       // $('#meet-the-team').css('background-position','0 0');
    }
}
